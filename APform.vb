﻿Imports Microsoft.Office.Interop.Excel
Imports Microsoft.VisualBasic
Imports System.Math
Imports System.String



Public Class APform

    Private Sub TBrfrom_TextChanged(sender As Object, e As EventArgs) Handles TBrfrom.TextChanged
        Dim activeWorksheet As Excel.Worksheet = CType(Globals.ThisAddIn.Application.ActiveSheet, Excel.Worksheet)
        On Error Resume Next
        activeWorksheet.Range(Me.TBrfrom.Text, Globals.ThisAddIn.Application.Selection.Cells(Globals.ThisAddIn.Application.Selection.Cells.Count).Address(False, False)).Select()
    End Sub

    Private Sub TBrto_TextChanged(sender As Object, e As EventArgs) Handles TBrto.TextChanged
        Dim activeWorksheet As Excel.Worksheet = CType(Globals.ThisAddIn.Application.ActiveSheet, Excel.Worksheet)
        On Error Resume Next
        activeWorksheet.Range(Globals.ThisAddIn.Application.Selection.Cells(1).Address(False, False), Me.TBrto.Text).Select()
    End Sub

End Class