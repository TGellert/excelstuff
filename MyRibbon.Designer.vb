﻿Partial Class MyRibbon
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'This call is required by the Component Designer.
        InitializeComponent()

    End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MyRibbon))
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.Attilas = Me.Factory.CreateRibbonGroup
        Me.BTNappstart = Me.Factory.CreateRibbonButton
        Me.Tab1.SuspendLayout()
        Me.Attilas.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.Attilas)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'Attilas
        '
        Me.Attilas.Items.Add(Me.BTNappstart)
        Me.Attilas.Label = "MyAddIns"
        Me.Attilas.Name = "Attilas"
        '
        'BTNappstart
        '
        Me.BTNappstart.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge
        Me.BTNappstart.Image = CType(resources.GetObject("BTNappstart.Image"), System.Drawing.Image)
        Me.BTNappstart.Label = "ArrayPrinter"
        Me.BTNappstart.Name = "BTNappstart"
        Me.BTNappstart.ShowImage = True
        '
        'MyRibbon
        '
        Me.Name = "MyRibbon"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.Attilas.ResumeLayout(False)
        Me.Attilas.PerformLayout()

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents Attilas As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents BTNappstart As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property MyRibbon() As MyRibbon
        Get
            Return Me.GetRibbon(Of MyRibbon)()
        End Get
    End Property
End Class
