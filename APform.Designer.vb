﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class APform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(APform))
        Me.Lrange = New System.Windows.Forms.Label()
        Me.Lcellnum = New System.Windows.Forms.Label()
        Me.Lvarname = New System.Windows.Forms.Label()
        Me.Larraysize = New System.Windows.Forms.Label()
        Me.Loffset = New System.Windows.Forms.Label()
        Me.Lfill = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBrfrom = New System.Windows.Forms.TextBox()
        Me.TBrto = New System.Windows.Forms.TextBox()
        Me.TBselected = New System.Windows.Forms.TextBox()
        Me.TBvarname = New System.Windows.Forms.TextBox()
        Me.TBarraysize = New System.Windows.Forms.TextBox()
        Me.TBoffset = New System.Windows.Forms.TextBox()
        Me.TBfill = New System.Windows.Forms.TextBox()
        Me.CBbycolumns = New System.Windows.Forms.CheckBox()
        Me.Lvartype = New System.Windows.Forms.Label()
        Me.CBvartype = New System.Windows.Forms.ComboBox()
        Me.CBconst = New System.Windows.Forms.CheckBox()
        Me.OBdec = New System.Windows.Forms.RadioButton()
        Me.OBhex8 = New System.Windows.Forms.RadioButton()
        Me.OBhex16 = New System.Windows.Forms.RadioButton()
        Me.OBhex32 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BTNbrowse = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Lrange
        '
        Me.Lrange.AutoSize = True
        Me.Lrange.Location = New System.Drawing.Point(20, 26)
        Me.Lrange.Name = "Lrange"
        Me.Lrange.Size = New System.Drawing.Size(42, 13)
        Me.Lrange.TabIndex = 0
        Me.Lrange.Text = "Range:"
        '
        'Lcellnum
        '
        Me.Lcellnum.AutoSize = True
        Me.Lcellnum.Location = New System.Drawing.Point(20, 55)
        Me.Lcellnum.Name = "Lcellnum"
        Me.Lcellnum.Size = New System.Drawing.Size(88, 13)
        Me.Lcellnum.TabIndex = 1
        Me.Lcellnum.Text = "Numbers of cells:"
        '
        'Lvarname
        '
        Me.Lvarname.AutoSize = True
        Me.Lvarname.Location = New System.Drawing.Point(20, 170)
        Me.Lvarname.Name = "Lvarname"
        Me.Lvarname.Size = New System.Drawing.Size(77, 13)
        Me.Lvarname.TabIndex = 2
        Me.Lvarname.Text = "Variable name:"
        '
        'Larraysize
        '
        Me.Larraysize.AutoSize = True
        Me.Larraysize.Location = New System.Drawing.Point(20, 201)
        Me.Larraysize.Name = "Larraysize"
        Me.Larraysize.Size = New System.Drawing.Size(55, 13)
        Me.Larraysize.TabIndex = 3
        Me.Larraysize.Text = "Array size:"
        '
        'Loffset
        '
        Me.Loffset.AutoSize = True
        Me.Loffset.Location = New System.Drawing.Point(20, 232)
        Me.Loffset.Name = "Loffset"
        Me.Loffset.Size = New System.Drawing.Size(76, 13)
        Me.Loffset.TabIndex = 4
        Me.Loffset.Text = "Position offset:"
        '
        'Lfill
        '
        Me.Lfill.AutoSize = True
        Me.Lfill.Location = New System.Drawing.Point(20, 263)
        Me.Lfill.Name = "Lfill"
        Me.Lfill.Size = New System.Drawing.Size(51, 13)
        Me.Lfill.TabIndex = 5
        Me.Lfill.Text = "Fill value:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(122, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "-"
        '
        'TBrfrom
        '
        Me.TBrfrom.Location = New System.Drawing.Point(67, 23)
        Me.TBrfrom.Name = "TBrfrom"
        Me.TBrfrom.Size = New System.Drawing.Size(49, 20)
        Me.TBrfrom.TabIndex = 7
        '
        'TBrto
        '
        Me.TBrto.Location = New System.Drawing.Point(138, 23)
        Me.TBrto.Name = "TBrto"
        Me.TBrto.Size = New System.Drawing.Size(49, 20)
        Me.TBrto.TabIndex = 8
        '
        'TBselected
        '
        Me.TBselected.Enabled = False
        Me.TBselected.Location = New System.Drawing.Point(114, 52)
        Me.TBselected.MaxLength = 0
        Me.TBselected.Name = "TBselected"
        Me.TBselected.Size = New System.Drawing.Size(49, 20)
        Me.TBselected.TabIndex = 9
        '
        'TBvarname
        '
        Me.TBvarname.Location = New System.Drawing.Point(103, 167)
        Me.TBvarname.Name = "TBvarname"
        Me.TBvarname.Size = New System.Drawing.Size(93, 20)
        Me.TBvarname.TabIndex = 10
        '
        'TBarraysize
        '
        Me.TBarraysize.Location = New System.Drawing.Point(103, 198)
        Me.TBarraysize.Name = "TBarraysize"
        Me.TBarraysize.Size = New System.Drawing.Size(49, 20)
        Me.TBarraysize.TabIndex = 11
        '
        'TBoffset
        '
        Me.TBoffset.Location = New System.Drawing.Point(103, 229)
        Me.TBoffset.Name = "TBoffset"
        Me.TBoffset.Size = New System.Drawing.Size(49, 20)
        Me.TBoffset.TabIndex = 12
        '
        'TBfill
        '
        Me.TBfill.Location = New System.Drawing.Point(103, 260)
        Me.TBfill.Name = "TBfill"
        Me.TBfill.Size = New System.Drawing.Size(49, 20)
        Me.TBfill.TabIndex = 13
        '
        'CBbycolumns
        '
        Me.CBbycolumns.AutoSize = True
        Me.CBbycolumns.Location = New System.Drawing.Point(23, 82)
        Me.CBbycolumns.Name = "CBbycolumns"
        Me.CBbycolumns.Size = New System.Drawing.Size(81, 17)
        Me.CBbycolumns.TabIndex = 14
        Me.CBbycolumns.Text = "By Columns"
        Me.CBbycolumns.UseVisualStyleBackColor = True
        '
        'Lvartype
        '
        Me.Lvartype.AutoSize = True
        Me.Lvartype.Location = New System.Drawing.Point(20, 113)
        Me.Lvartype.Name = "Lvartype"
        Me.Lvartype.Size = New System.Drawing.Size(71, 13)
        Me.Lvartype.TabIndex = 15
        Me.Lvartype.Text = "Variable type:"
        '
        'CBvartype
        '
        Me.CBvartype.FormattingEnabled = True
        Me.CBvartype.Location = New System.Drawing.Point(94, 110)
        Me.CBvartype.Name = "CBvartype"
        Me.CBvartype.Size = New System.Drawing.Size(93, 21)
        Me.CBvartype.TabIndex = 16
        '
        'CBconst
        '
        Me.CBconst.AutoSize = True
        Me.CBconst.Location = New System.Drawing.Point(23, 141)
        Me.CBconst.Name = "CBconst"
        Me.CBconst.Size = New System.Drawing.Size(74, 17)
        Me.CBconst.TabIndex = 17
        Me.CBconst.Text = "Constant?"
        Me.CBconst.UseVisualStyleBackColor = True
        '
        'OBdec
        '
        Me.OBdec.AutoSize = True
        Me.OBdec.Location = New System.Drawing.Point(100, 140)
        Me.OBdec.Name = "OBdec"
        Me.OBdec.Size = New System.Drawing.Size(43, 17)
        Me.OBdec.TabIndex = 18
        Me.OBdec.TabStop = True
        Me.OBdec.Text = "dec"
        Me.OBdec.UseVisualStyleBackColor = True
        '
        'OBhex8
        '
        Me.OBhex8.AutoSize = True
        Me.OBhex8.Location = New System.Drawing.Point(149, 140)
        Me.OBhex8.Name = "OBhex8"
        Me.OBhex8.Size = New System.Drawing.Size(48, 17)
        Me.OBhex8.TabIndex = 19
        Me.OBhex8.TabStop = True
        Me.OBhex8.Text = "hex8"
        Me.OBhex8.UseVisualStyleBackColor = True
        '
        'OBhex16
        '
        Me.OBhex16.AutoSize = True
        Me.OBhex16.Location = New System.Drawing.Point(203, 140)
        Me.OBhex16.Name = "OBhex16"
        Me.OBhex16.Size = New System.Drawing.Size(54, 17)
        Me.OBhex16.TabIndex = 20
        Me.OBhex16.TabStop = True
        Me.OBhex16.Text = "hex16"
        Me.OBhex16.UseVisualStyleBackColor = True
        '
        'OBhex32
        '
        Me.OBhex32.AutoSize = True
        Me.OBhex32.Location = New System.Drawing.Point(263, 140)
        Me.OBhex32.Name = "OBhex32"
        Me.OBhex32.Size = New System.Drawing.Size(54, 17)
        Me.OBhex32.TabIndex = 21
        Me.OBhex32.TabStop = True
        Me.OBhex32.Text = "hex32"
        Me.OBhex32.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 298)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Output file:"
        '
        'BTNbrowse
        '
        Me.BTNbrowse.Location = New System.Drawing.Point(84, 287)
        Me.BTNbrowse.Name = "BTNbrowse"
        Me.BTNbrowse.Size = New System.Drawing.Size(80, 35)
        Me.BTNbrowse.TabIndex = 23
        Me.BTNbrowse.Text = "Browse.."
        Me.BTNbrowse.UseVisualStyleBackColor = True
        '
        'APform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 344)
        Me.Controls.Add(Me.BTNbrowse)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.OBhex32)
        Me.Controls.Add(Me.OBhex16)
        Me.Controls.Add(Me.OBhex8)
        Me.Controls.Add(Me.OBdec)
        Me.Controls.Add(Me.CBconst)
        Me.Controls.Add(Me.CBvartype)
        Me.Controls.Add(Me.Lvartype)
        Me.Controls.Add(Me.CBbycolumns)
        Me.Controls.Add(Me.TBfill)
        Me.Controls.Add(Me.TBoffset)
        Me.Controls.Add(Me.TBarraysize)
        Me.Controls.Add(Me.TBvarname)
        Me.Controls.Add(Me.TBselected)
        Me.Controls.Add(Me.TBrto)
        Me.Controls.Add(Me.TBrfrom)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Lfill)
        Me.Controls.Add(Me.Loffset)
        Me.Controls.Add(Me.Larraysize)
        Me.Controls.Add(Me.Lvarname)
        Me.Controls.Add(Me.Lcellnum)
        Me.Controls.Add(Me.Lrange)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "APform"
        Me.Text = "Array Printer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Lrange As System.Windows.Forms.Label
    Friend WithEvents Lcellnum As System.Windows.Forms.Label
    Friend WithEvents Lvarname As System.Windows.Forms.Label
    Friend WithEvents Larraysize As System.Windows.Forms.Label
    Friend WithEvents Loffset As System.Windows.Forms.Label
    Friend WithEvents Lfill As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TBrto As System.Windows.Forms.TextBox
    Friend WithEvents TBselected As System.Windows.Forms.TextBox
    Friend WithEvents TBvarname As System.Windows.Forms.TextBox
    Friend WithEvents TBarraysize As System.Windows.Forms.TextBox
    Friend WithEvents TBoffset As System.Windows.Forms.TextBox
    Friend WithEvents TBfill As System.Windows.Forms.TextBox
    Friend WithEvents CBbycolumns As System.Windows.Forms.CheckBox
    Friend WithEvents Lvartype As System.Windows.Forms.Label
    Friend WithEvents CBvartype As System.Windows.Forms.ComboBox
    Friend WithEvents CBconst As System.Windows.Forms.CheckBox
    Friend WithEvents OBdec As System.Windows.Forms.RadioButton
    Friend WithEvents OBhex8 As System.Windows.Forms.RadioButton
    Friend WithEvents OBhex16 As System.Windows.Forms.RadioButton
    Friend WithEvents OBhex32 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BTNbrowse As System.Windows.Forms.Button
    Public WithEvents TBrfrom As System.Windows.Forms.TextBox
End Class
